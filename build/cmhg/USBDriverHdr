; Copyright 2017 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
#include "Global/Services.h"
#include "../VersionNum"

initialisation-code:    module_init

finalisation-code:      module_final

service-call-handler:   module_services Service_ResourceFSStarting,
                                        Service_DeviceFSStarting,
                                        Service_DeviceFSDying,
                                        Service_USB,
                                        Service_PreReset
title-string:           USBDriver

help-string:            USBDriver Module_MajorVersion_CMHG

date-string:            Module_Date_CMHG

international-help-file: "Resources:$.Resources.USBDriver.Messages"

command-keyword-table:  module_commands

#ifdef USB_DEBUG
USBDebug(       min-args:1, max-args:3,
                help-text:     "*USBDebug sets the debug level for general usb, hub and mouse output.",
                invalid-syntax:"*USBDebug <usb> [<hub> [<mouse>]]"),
#endif

USBBuses(       min-args:0, max-args:0,
                international:,help-text:"HUSBBUS",invalid-syntax:"SUSBBUS"),

USBDevices(     min-args:0, max-args:0,
                international:,help-text:"HUSBDEV",invalid-syntax:"SUSBDEV"),

USBDevInfo(     min-args:1, max-args:1,
                international:,help-text:"HUSBDIN",invalid-syntax:"SUSBDIN"),

USBConfInfo(    min-args:1, max-args:1,
                international:,help-text:"HUSBCIN",invalid-syntax:"SUSBCIN"),

USBSetConfig(   min-args:2, max-args:2,
                international:,help-text:"HUSBCON",invalid-syntax:"SUSBCON"),

USBSetInterface(min-args:3, max-args:3,
                international:,help-text:"HUSBINT",invalid-syntax:"SUSBINT"),

#ifdef USB_DEBUG
USBDiscover(    min-args:0, max-args:0,
                help-text:     "*USBDiscover forces a search for new devices on all buses.",
                invalid-syntax:"Syntax: *USBDiscover"),
#endif

USBReset(       min-args:1, max-args:1,
                international:,help-text:"HUSBRES",invalid-syntax:"SUSBRES"),

USBQuirk(       min-args:1, max-args:5,
                international:,help-text:"HUSBQRK",invalid-syntax:"SUSBQRK")

swi-chunk-base-number:  0x54a40

swi-handler-code:       module_swis

swi-decoding-table:     USBDriver,
                        RegisterBus,
                        DeregisterBus,
                        InsertTransfer,
                        TransferComplete,
                        ScheduleSoftInterrupt,
                        Version

generic-veneers:        driver_entry/driver

vector-handlers:        pointerv_entry/pointerv,
                        keyv_entry/keyv,
                        softintr_entry/softintr
